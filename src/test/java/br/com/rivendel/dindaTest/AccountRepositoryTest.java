package br.com.rivendel.dindaTest;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import br.com.rivendel.dindaTest.exceptions.RecordException;
import br.com.rivendel.dindaTest.model.Account;
import br.com.rivendel.dindaTest.model.Transaction;

public class AccountRepositoryTest extends BaseTest {

	@Test(expected=IOException.class)
	public void testFindAccountsInvalid() throws IOException, RecordException {
		accountRepository.findAccounts("");
	}

	@Test
	public void testFindAccounts() throws IOException, RecordException {
		final List<Account> accounts = accountRepository.findAccounts(ACCOUNTS_FILE);
		
		assertNotNull("Accounts is null", accounts);
		assertTrue("Empty accounts", accounts.size() > 0);
	}
	
	@Test(expected=IOException.class)
	public void testFindTransactionsInvalid() throws IOException, RecordException {
		accountRepository.findTransactions("");
	}

	@Test
	public void testFindTransactionsValid() throws IOException, RecordException {
		final List<Transaction> transactions = accountRepository.findTransactions(TRANSACTIONS_FILE);
		
		assertNotNull("Transactions is null", transactions);
		assertTrue("Empty transactions", transactions.size() > 0);
	}
}
