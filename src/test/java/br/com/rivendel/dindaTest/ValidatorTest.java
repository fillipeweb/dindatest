package br.com.rivendel.dindaTest;
import org.junit.Test;

import br.com.rivendel.dindaTest.validator.Validator;

public class ValidatorTest {
	
	@Test(expected=Exception.class)
	public void testAccountNumberInvalid() throws Exception {
		Validator.accountNumber("22222d");
	}
	
	@Test
	public void testAccountNumberValid() throws Exception {
		Validator.accountNumber("2222");
	}
	
	@Test(expected=Exception.class)
	public void testMoneyInvalid() throws Exception {
		Validator.money("12.333");
	}
	
	@Test
	public void testMoneyValid() throws Exception {
		Validator.accountNumber("2222");
	}

}
