package br.com.rivendel.dindaTest;
import static org.junit.Assert.*;
import java.io.IOException;
import java.util.List;

import org.joda.money.Money;
import org.junit.Test;

import br.com.rivendel.dindaTest.exceptions.RecordException;
import br.com.rivendel.dindaTest.facade.BalanceFacade;
import br.com.rivendel.dindaTest.model.Account;

public class BalanceFacadeTest extends BaseTest {

	private final BalanceFacade balanceFacade = new BalanceFacade();

	@Test
	public void testApplyTransaction() {
		final Money resultExpected = Money.of(Account.CURRENCY_BRL, -45.25);
		final Money balance = Money.of(Account.CURRENCY_BRL, -35.25);
		final Money transaction = Money.of(Account.CURRENCY_BRL, -5.0);
		final Money applyTransaction = balanceFacade.applyTransaction(balance,
				transaction);

		assertTrue("Calculation is not correct.", applyTransaction.isEqual(resultExpected));
	}

	@Test
	public void testCalculate() throws IOException, RecordException {
		final List<Account> resultExpected = readResult();
		final List<Account> calculate = balanceFacade.calculate(ACCOUNTS_FILE,
				TRANSACTIONS_FILE);
		
		assert calculate != null;
		assert calculate.size() > 0;
		
		for (Account account : calculate) {
			final Integer id = account.getId();
			final Money balance = account.getBalance();
			for (Account result : resultExpected) {
				if (result.getId().equals(id))
					assertTrue("Any balance is not correct.", result.getBalance().isEqual(balance));
			}
		}
	}

}
