package br.com.rivendel.dindaTest;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import br.com.rivendel.dindaTest.exceptions.RecordException;
import br.com.rivendel.dindaTest.model.Account;
import br.com.rivendel.dindaTest.repository.AccountRepository;

public class BaseTest {
	
	static {
		Locale.setDefault(new Locale("pt", "BR"));
	}
	
	protected final String ACCOUNTS_FILE = "resources/accounts.csv";
	protected final String TRANSACTIONS_FILE = "resources/transactions.csv";
	protected final String RESULT_FILE = "resources/result.csv";

	protected final AccountRepository accountRepository = new AccountRepository();

	protected List<Account> readResult() throws IOException, RecordException {
		return accountRepository.findAccounts(RESULT_FILE);
	}

}
