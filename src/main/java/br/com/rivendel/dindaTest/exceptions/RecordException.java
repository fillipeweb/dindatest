package br.com.rivendel.dindaTest.exceptions;

public class RecordException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private Long recordNumber;
	
	public RecordException(Long recordNumber, String message) {
		super(String.format("Record %s: %s", recordNumber, message));
		this.recordNumber = recordNumber;
	}

	public Long getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(Long recordNumber) {
		this.recordNumber = recordNumber;
	}

}
