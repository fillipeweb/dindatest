package br.com.rivendel.dindaTest.model;

import org.joda.money.Money;

public class Transaction extends Account {

	public Transaction() {

	}

	public Transaction(Integer id, Money balance) {
		super(id, balance);
	}

	public Transaction(String id, String balance) {
		super(id, balance);
	}

}
