package br.com.rivendel.dindaTest.model;

import java.util.Locale;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;

public class Account {
	
	public static final CurrencyUnit CURRENCY_BRL = CurrencyUnit.of(Locale.getDefault());

	private Integer id;

	private Money balance = Money.of(CURRENCY_BRL, 0.0);

	public Account() {

	}

	public Account(Integer id, Money balance) {
		this.id = id;
		this.balance = balance;
	}

	public Account(String id, String balance) {
		this.id = Integer.valueOf(id);
		this.balance = Money.of(CURRENCY_BRL, Integer.valueOf(balance) / 100.00);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Money getBalance() {
		return balance;
	}
	
	public Integer getBalanceInCents() {
		return balance.multipliedBy(100).getAmount().intValue();
	}

	public void setBalance(Money balance) {
		this.balance = balance;
	}

}
