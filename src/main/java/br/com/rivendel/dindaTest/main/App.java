package br.com.rivendel.dindaTest.main;

import java.util.List;
import java.util.Locale;

import br.com.rivendel.dindaTest.facade.BalanceFacade;
import br.com.rivendel.dindaTest.model.Account;
import br.com.rivendel.dindaTest.util.SystemMessageHandler;

/**
 * Balance calculator
 *
 */
public class App {

	static {
		Locale.setDefault(new Locale("pt", "BR"));
	}

	public static void main(String[] args) {

		if (args.length < 2)
			SystemMessageHandler.error("Params are invalid.");

		try {
			final BalanceFacade balanceFacade = new BalanceFacade();
			final List<Account> balance = balanceFacade.calculate(args[0],
					args[1]);

			for (Account account : balance) {
				System.out.println(String.format("%s,%s", account.getId(),
						account.getBalanceInCents()));
			}
		} catch (Exception e) {
			SystemMessageHandler.error("Error reading file: ", e);
		}
	}
}
