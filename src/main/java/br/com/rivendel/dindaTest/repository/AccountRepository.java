package br.com.rivendel.dindaTest.repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVRecord;

import br.com.rivendel.dindaTest.exceptions.RecordException;
import br.com.rivendel.dindaTest.model.Account;
import br.com.rivendel.dindaTest.model.Transaction;
import br.com.rivendel.dindaTest.util.ReaderCSV;
import br.com.rivendel.dindaTest.validator.Validator;

public class AccountRepository {
	
	public List<Account> findAccounts(String file) throws IOException, RecordException {
		final Iterable<CSVRecord> records = ReaderCSV.init().file(file).rfc4180().build();
		final List<Account> accounts = new ArrayList<Account>();
		
		for (CSVRecord csvRecord : records) {
			String accountNumber = csvRecord.get(0).trim();
			String balance = csvRecord.get(1).trim();
			
			try {
				Validator.accountNumber(accountNumber);
				Validator.money(balance);
			} catch (Exception e) {
				throw new RecordException(csvRecord.getRecordNumber(), e.getMessage());
			}
			
			accounts.add(new Account(accountNumber, balance));
		}
		
		return accounts;
	}
	
	public List<Transaction> findTransactions(String file) throws IOException, RecordException {
		final Iterable<CSVRecord> records = ReaderCSV.init().file(file).rfc4180().build();
		final List<Transaction> transactions = new ArrayList<Transaction>();
		
		for (CSVRecord csvRecord : records) {
			String accountNumber = csvRecord.get(0).trim();
			String balance = csvRecord.get(1).trim();
			
			try {
				Validator.accountNumber(accountNumber);
				Validator.money(balance);
			} catch (Exception e) {
				throw new RecordException(csvRecord.getRecordNumber(), e.getMessage());
			}
			
			transactions.add(new Transaction(accountNumber, balance));
		}
		
		return transactions;
	}

}
