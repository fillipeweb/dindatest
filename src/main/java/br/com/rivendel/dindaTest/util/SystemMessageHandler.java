package br.com.rivendel.dindaTest.util;

public class SystemMessageHandler {
	
	public static void error(String message, Exception e) {
		
		if (message == null)
			System.exit(-1);
		
		String errorMessage = null;
		if (e != null)
			errorMessage = String.format("%s:%s", message, e.getMessage());
		else
			errorMessage = String.format(message);
			
		System.out.println(errorMessage);
		System.exit(-1);
	}
	
	public static void error(String message) {
		error(message, null);
	}
	
	public static void ok(String message) {
		if (message == null)
			System.exit(0);
		
		System.out.println(message);
		System.exit(0);
	}

}
