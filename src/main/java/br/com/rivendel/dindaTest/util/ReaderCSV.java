package br.com.rivendel.dindaTest.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class ReaderCSV {
	
	private Reader in;
	private CSVFormat format;
	
	public static ReaderCSV init() {
		return new ReaderCSV();
	}
	
	public Format file(String filePath) throws FileNotFoundException {
		in = new FileReader(filePath);
		
		return new Format();
	}
	
	public class Format {
		
		public Build excel() {
			format = CSVFormat.EXCEL;
			
			return new Build();
		}
		
		public Build rfc4180() {
			format = CSVFormat.RFC4180;
			
			return new Build();
		}
	}
	
	public class Build {
		
		public Iterable<CSVRecord> build() throws IOException {
			return format.parse(in);
		}
	}
}
