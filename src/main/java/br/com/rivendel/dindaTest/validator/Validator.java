package br.com.rivendel.dindaTest.validator;

import org.joda.money.Money;

import br.com.rivendel.dindaTest.model.Account;


public class Validator {
	
	public static void accountNumber(String accountNumber) throws Exception {
		try {
			Integer.valueOf((String) accountNumber);
		} catch (NumberFormatException e) {
			throw new Exception("Account number is not valid");
		}
	}
	
	public static void money(String money) throws Exception {
		try {
			Money.of(Account.CURRENCY_BRL, Integer.valueOf(money) / 100.00);
		} catch (NumberFormatException e) {
			throw new Exception("Invalid format for money. Must be 2000 = 20,00.");
		}
	}

}
