package br.com.rivendel.dindaTest.facade;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joda.money.Money;

import br.com.rivendel.dindaTest.exceptions.RecordException;
import br.com.rivendel.dindaTest.model.Account;
import br.com.rivendel.dindaTest.model.Transaction;
import br.com.rivendel.dindaTest.repository.AccountRepository;

public class BalanceFacade {

	private final static Money FINE = Money.of(Account.CURRENCY_BRL,-5.0);

	private final AccountRepository accountRepository = new AccountRepository();

	public List<Account> calculate(String fileAccounts, String fileTransactions)
			throws IOException, RecordException {
		final List<Account> accounts = accountRepository
				.findAccounts(fileAccounts);
		final List<Transaction> transactions = accountRepository
				.findTransactions(fileTransactions);

		final List<Account> result = new ArrayList<Account>();
		for (Account account : accounts) {
			Money balance = account.getBalance();
			Integer id = account.getId();
			for (Transaction transaction : transactions) {
				if (transaction.getId().equals(id))
					balance = applyTransaction(balance, transaction.getBalance());
			}

			account.setBalance(balance);
			result.add(account);
		}
		
		return result;
	}

	public Money applyTransaction(Money balance, Money transaction) {
		Money newBalance = balance.plus(transaction);

		if (transaction.isNegative() && newBalance.isNegative())
			newBalance = newBalance.plus(FINE);
		
		return newBalance;
	}

}
