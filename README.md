# App teste do exercicio da Dinda (Java 1.7) #

## Como Gerar o Projeto ##

```
#!bash

mvn install
```
## Como executar o projeto ##

```
#!bash

java -jar target/dindaTest-0.0.1-jar-with-dependencies.jar resources/accounts.csv resources/transactions.csv
```
# Frameworks Utilizados #
* Junit
* Apache Commons CSV
* Joda Money